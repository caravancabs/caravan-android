/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.caravan.app;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.caravan.PagerSlidingTabStrip;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taxidemo.DetailView.DetailActivity;
import taxidemo.TaxiApp;

public class MainActivity extends ActionBarActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tabs)
    PagerSlidingTabStrip tabs;
    @InjectView(R.id.pager)
    ViewPager pager;
    public static ProgressBar pb;
    private MyPagerAdapter adapter;
    private Drawable oldBackground = null;
    private int currentColor;
    private SystemBarTintManager mTintManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        //setTitle(TaxiApp.destAddress);
        setTitle(Html.fromHtml("<small>"+TaxiApp.destAddress+"</small>"));
        // create our manager instance after the content view is set
        mTintManager = new SystemBarTintManager(this);
        // enable status bar tint
        mTintManager.setStatusBarTintEnabled(true);
        adapter = new MyPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        tabs.setViewPager(pager);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        //pager.setPageMargin(pageMargin);



        pager.setCurrentItem(0);
        changeColor(getResources().getColor(R.color.Black));

        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
              //  Toast.makeText(MainActivity.this, "reselected: " + position, Toast.LENGTH_SHORT).show();
            }
        });

        pb = (ProgressBar) findViewById(R.id.progb123);
        pb.setVisibility(View.VISIBLE);

        // Create a Handler instance on the main thread
        final Handler handler = new Handler();

// Create and start a new Thread
        new Thread(new Runnable() {
            public void run() {
                try{
                    Thread.sleep(15000);
                }
                catch (Exception e) { } // Just catch the InterruptedException

                // Now we use the Handler to post back to the main thread
                handler.post(new Runnable() {
                    public void run() {
                        // Set the View's visibility back on the main UI Thread
                        pb.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).start();
        //ProgBarHand pp = new ProgBarHand();
        //pp.execute("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static void hideProgressBar(){
        pb.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
              //  QuickTaxiFragment.newInstance().show(getSupportFragmentManager(), "QuickTaxiFragment");
                startActivity(new Intent(this, DetailActivity.class));
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeColor(int newColor) {
        tabs.setBackgroundColor(newColor);
        mTintManager.setTintColor(newColor);
        // change ActionBar color just if an ActionBar is available
        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(getResources().getColor(android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {
            getSupportActionBar().setBackgroundDrawable(ld);
        } else {
            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);
        }

        oldBackground = ld;
        currentColor = newColor;
    }

    public void onColorClicked(View v) {
        int color = Color.parseColor(v.getTag().toString());
        changeColor(color);
       /* if(v.getId()==R.id.hatch)
        {

        }*/
  /*      TaxiApp taxiApp=new TaxiApp();
        final TaxiAdapter taxiAdapter= taxiApp.getUberTaxiAdapter();

        int count=taxiAdapter.getCount();
        int a[]=new int[count];
        for(int i=0;i<count-2;i++)
        {
            a[i]=taxiAdapter.getItem(i).getTimeInSec();

        }
        for(int i=0;i<count;i++)
        {
            for(int j=0;j<count-i;j++)
            {
                if(a[j]>a[j+1])
                {
                    int temp=a[j];
                    a[j]=a[j+1];
                    a[j+1]=temp;
                }
            }
        }

        for(int i=0;i<count-1;i++)
        {
            if(taxiAdapter.getItem(i).getTimeInSec().equals(a[i]))
                TaxiApp.etaAdapter.add(taxiAdapter.getItem(i));
        }
        TaxiApp.etaAdapter.notifyDataSetChanged();*/
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentColor", currentColor);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentColor = savedInstanceState.getInt("currentColor");
        changeColor(currentColor);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"All    ","Ola  ","Uber   ", "TaxiForSure","Meru"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {


            return  SuperAwesomeCardFragment.newInstance(position);


        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

   @Override
   public void onBackPressed() {
       startActivity(new Intent(this, DetailActivity.class));
       finish();
   }

    public class ProgBarHand extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            Log.d("progress", "aaaaaaaaaaa");
           // pb.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            int progressStatus = 0;
            while (progressStatus < 12000) {
                progressStatus++;
                try {
                    Thread.sleep(1);
                    Log.d("progress", ""+progressStatus);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("progress","aaaaaaaaaaa");
            pb.setVisibility(View.INVISIBLE);
            super.onPostExecute(s);
        }
    }
}