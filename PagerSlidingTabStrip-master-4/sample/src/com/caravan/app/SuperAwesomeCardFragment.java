
package com.caravan.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taxidemo.TaxiApp;
import taxidemo.cabs.Meru;
import taxidemo.cabs.Ola;
import taxidemo.cabs.TaxiForSure;
import taxidemo.cabs.Uber;
import taxidemo.db.model.TaxiTime;
import taxidemo.mainview.TaxiAdapter;

public class SuperAwesomeCardFragment extends Fragment {

	private static final String ARG_POSITION = "position";

	TaxiAdapter adapter;
	private Location location,destLocation;
	ViewGroup container;
	private int position;
	TaxiApp taxiapp;
	@InjectView(R.id.list)
	ListView listView;
	@InjectView(R.id.header)
	TextView textView;
	//@InjectView(R.id.header1)
	//TextView textView1;
	@InjectView(R.id.header2)
	TextView textView2;
	@InjectView(R.id.header3)
	TextView textView3;

	@InjectView(R.id.swipyrefreshlayout)
	SwipyRefreshLayout swipyRefreshLayout;

	TaxiAdapter sureAdapter = null;
	int startActivity=0;
	boolean isRefresh=true;
	public static SuperAwesomeCardFragment newInstance(int position) {
		SuperAwesomeCardFragment f = new SuperAwesomeCardFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		position = getArguments().getInt(ARG_POSITION);

		if(savedInstanceState==null) {
			taxiapp = (TaxiApp) getActivity().getApplicationContext();

			location = taxiapp.getSrcLocation();
			destLocation = taxiapp.getDestLocation();
			adapter=taxiapp.getUberTaxiAdapter();

			if(TaxiApp.refresh) {
				adapter.clear();


				getData();
				taxiapp.setRefresh();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_card,container,false);

		ButterKnife.inject(this, rootView);
		ViewCompat.setElevation(rootView, 50);

	//	textView.setText("Taxi");

	//	textView1.setText(" Model");
		textView2.setText("  ETA");
		textView3.setText("Price     ");

		Log.d("Adapter", "set");
		swipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
		swipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh(SwipyRefreshLayoutDirection swipyRefreshLayoutDirection) {

				try {
					adapter.clear();
					getData();
				} catch (Exception e) {
				}
			}
		});




		 AdapterView.OnItemClickListener onItemClickListener=new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				final TaxiTime taxiTime = (TaxiTime) parent.getAdapter().getItem(position);
				QuickTaxiFragment quickTaxiFragment=QuickTaxiFragment.newInstance();
				quickTaxiFragment.image=R.drawable.contact;
				String model=taxiTime.getCompanyName()+"("+taxiTime.getModelName()+")";
				String company=taxiTime.getCompanyName();
				quickTaxiFragment.taxiName=model;
				quickTaxiFragment.s_eta = (taxiTime.getTimeInSec()/60)+" min";
				quickTaxiFragment.s_price = taxiTime.getPrice();
				Context context=getActivity().getApplicationContext();
				String packageName="";
				if(company== "Uber Cabs")
				{
					packageName="com.ubercab";
				}
				else if (company=="TaxiForSure")
				{
					packageName="com.tfs.consumer";
				}
				else if (company == "Ola")
				{
					packageName="com.olacabs.customer";
				}
				else if(company=="Meru Cabs")
				{
					packageName="com.winit.merucab";
				}


				quickTaxiFragment.packageName=packageName;
				//  REDIRECT TO APP
				quickTaxiFragment.installed  =   appInstalledOrNot(packageName);
				quickTaxiFragment.show(getFragmentManager(),"SUV");

			}
			 private boolean appInstalledOrNot(String uri) {
				 PackageManager pm = getActivity().getApplicationContext().getPackageManager();
				 boolean app_installed = false;
				 try {
					 pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
					 app_installed = true;
				 }
				 catch (PackageManager.NameNotFoundException e) {
					 app_installed = false;
				 }
				 return app_installed ;
			 }


			 //		QuickTaxiFragment.newInstance().show(getFragmentManager(), "QuickFragment");


		};

		listView.setOnItemClickListener(onItemClickListener);
	//	adapter.notifyDataSetChanged();



		TextView all= (TextView) getActivity().findViewById(R.id.all);
		all.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				TaxiAdapter taxiAdapter=taxiapp.getUberTaxiAdapter();
				listView.setAdapter(taxiAdapter);
				taxiAdapter.notifyDataSetChanged();
			}
		});


		LinearLayout hatch= (LinearLayout) getActivity().findViewById(R.id.hatch);
	final LinearLayout sedan= (LinearLayout) getActivity().findViewById(R.id.sedan);
		final LinearLayout prime= (LinearLayout) getActivity().findViewById(R.id.suv);
		sedan.setClickable(true);
		sedan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Vibrator vi = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
				vi.vibrate(100);
				Log.d("Doing", "Sedan");
				listView.setAdapter(TaxiApp.sedanAdapter);
				TaxiApp.sedanAdapter.notifyDataSetChanged();
			}
		});
		hatch.setClickable(true);
		hatch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.d("Doing ", "Hatch");
				Vibrator vi = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
				vi.vibrate(100);
				listView.setAdapter(TaxiApp.hatchAdapter);
				TaxiApp.hatchAdapter.setNotifyOnChange(true);

			}
		});
		prime.setClickable(true);
		prime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Vibrator vi = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
				vi.vibrate(100);
				//listView.setAdapter(TaxiApp.primeAdapter);
				//TaxiApp.priceAdapter.setNotifyOnChange(true);
			}
		});


		if(position==0) {

			try {

				listView.setAdapter(adapter);

			}
			catch (Exception e){}
		}

		else if (position==3)
		{


			listView.setAdapter(TaxiApp.sureAdapter);
		}
		else if (position==2)
		{
			listView.setAdapter(TaxiApp.uberAdapter);
		}
		else if(position==1)
		{
			listView.setAdapter(TaxiApp.olaAdapter);
		}
		else if(position==4)
		{
			listView.setAdapter(TaxiApp.meruAdapter);
		}

		return rootView;
	}



	private void getDataFromUBER() {



		// get taxi list from Uber
		new AsyncTask<Location,Void,Boolean>()
		{
			@Override
			protected void onPreExecute() {
				//    pb.setVisibility(View.VISIBLE);
				//  findViewById(R.id.empty).setVisibility(View.GONE);
				Log.e("Uber", "Progressbar Visibility set");
			}

			@Override
			protected Boolean doInBackground(Location... params) {
				try {
					Uber uber = new Uber(params[0]);
					uber.putTaxiTimeInList(TaxiApp.gettaxiTimes(""));
					uber.putTaxiPriceInList(TaxiApp.gettaxiPrices(),location.getLatitude(),location.getLongitude(),destLocation.getLatitude(),destLocation.getLongitude());
					Log.e("Uber", "taxi put in list");

					return true;
				}catch (Exception e){
					Log.e("Uber", "No taxi added to list");
					return false;

				}

			}

			@Override
			protected void onPostExecute(Boolean result) {
				// pb.setVisibility(View.GONE);
				if(TaxiApp.gettaxiTimes("uber").size()==0)
					//   findViewById(R.id.empty).setVisibility(View.VISIBLE);
					if(result!=null && result==true)
						adapter.notifyDataSetChanged();
					else
					{
						Log.d("failed","uber");
						//    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

					}


			}
		}.execute(location);
	}

	// Getting data from Meru
	private void getDataFromMeru() {

		// get taxi list from Meru
		new AsyncTask<Location,Void,Boolean>()
		{

			@Override
			protected Boolean doInBackground(Location... params) {
				try {
					Meru meru=new Meru(params[0]);
					meru.putTaxiTimeInList(TaxiApp.gettaxiTimes(""));
					Log.e("", "taxi put in list Meru");
					return true;
				}catch (Exception e){
					Log.e("", "No taxi added to list Meru");
					e.printStackTrace();
					return false;
				}

			}

			@Override
			protected void onPostExecute(Boolean result) {
				//   pb.setVisibility(View.GONE);

				if(TaxiApp.gettaxiTimes("").size()==0)
					//     findViewById(R.id.empty).setVisibility(View.VISIBLE);
					if(result!=null && result==true){
						//   findViewById(R.id.empty).setVisibility(View.GONE);
						adapter.notifyDataSetChanged();

					}
					else
					{
						Log.d("failed","Meru");
						//    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

					}

			}
		}.execute(location);
	}
//End data

	//Getting data from Ola

	private void getDataFromOla() {

		// get taxi list from Ola
		new AsyncTask<Location,Void,Boolean>()
		{

			@Override
			protected Boolean doInBackground(Location... params) {
				try {
					Ola ola=new Ola(getActivity().getApplicationContext(),params[0]);
					ola.olaAccess(adapter);
					return true;
				}catch (Exception e){
					Log.e("Tag", "No taxi added to list Ola");
					e.printStackTrace();
					return false;
				}

			}

			@Override
			protected void onPostExecute(Boolean result) {
				if(TaxiApp.gettaxiTimes("uber").size()==0)
				//            findViewById(R.id.empty).setVisibility(View.VISIBLE);
				//    if(result!=null && result==true)
				//          adapter.notifyDataSetChanged();

				{
					Log.d("failed", "Ola");
					//    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

				}

			}
		}.execute(location);
	}


	// Option menu


	// Getting data from taxiforsure
	private void getDataFromTaxiForSure() {

		// get taxi list from TaxiForSure
		new AsyncTask<Location,Void,Boolean>()
		{

			@Override
			protected Boolean doInBackground(Location... params) {
				try {
					TaxiForSure taxiForSure=new TaxiForSure(params[0]);
					taxiForSure.getTaxiForSure(TaxiApp.gettaxiTimes(""));
					Log.e("Tag", "taxi put in list TaxiForSure");
					return true;
				}catch (Exception e){
					Log.e("Tag", "No taxi added to list TaxiForSure");
					e.printStackTrace();
					return false;
				}

			}

			@Override
			protected void onPostExecute(Boolean result) {
//                pb.setVisibility(View.GONE);
				if(TaxiApp.gettaxiTimes("taxiforsure").size()==0)
					//                   findViewById(R.id.empty).setVisibility(View.VISIBLE);
					if(result!=null && result==true)
						adapter.notifyDataSetChanged();
					else
					{
						Log.d("failed","Taxiforsure");
						//    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();


					}

			}
		}.execute(location);
	}
//End data

	public void getData()
	{
		TaxiApp.gettaxiPrices().clear();
		getDataFromUBER();
		getDataFromOla();
		getDataFromTaxiForSure();
		getDataFromMeru();
		//


	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().finish();
	}


}
