package com.caravan.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.caravan.PagerSlidingTabStrip;

public class QuickTaxiFragment extends DialogFragment {

    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
   // private ContactPagerAdapter adapter;
   // ImageView imageView;
    TextView bBook,bCancel;
    boolean installed;
    public int image;
    public String taxiName,packageName;
    public String s_price,s_eta;
    View root;
    public static QuickTaxiFragment newInstance() {
        QuickTaxiFragment quickContactFragment = new QuickTaxiFragment();
        return quickContactFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getDialog() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }

        root = inflater.inflate(R.layout.fragment_quick_contact, container, false);
      //  imageView= (ImageView) root.findViewById(R.id.image);

        bBook= (TextView) root.findViewById(R.id.book_button);
        final Context context=getActivity().getApplicationContext();
        bBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator vi = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vi.vibrate(100);
                if (installed) {
                    //This intent will help you to launch if the package is already installed
                    Intent LaunchIntent = context.getPackageManager()
                            .getLaunchIntentForPackage(packageName);
                    context.startActivity(LaunchIntent);

                    System.out.println("App already installed on your phone");
                } else {
                    System.out.println("App is not installed on your phone");
                    try {
                        //context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(")));
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    } catch (android.content.ActivityNotFoundException anfe) {
                        Log.d("Not", "Accessible");
                    }
                }

            }
        });
        bCancel =(TextView) root.findViewById(R.id.button_cancel);
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator vi = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vi.vibrate(100);
                dismiss();
            }
        });
       // imageView.setImageResource(image);
        TextView textView= (TextView) root.findViewById(R.id.taxi_name);
        TextView pr= (TextView) root.findViewById(R.id.tv_price_d);
        TextView et= (TextView) root.findViewById(R.id.tv_eta_d);
        textView.setText(taxiName);
        pr.setText(s_price);
        et.setText(s_eta);
        tabs = (PagerSlidingTabStrip) root.findViewById(R.id.tabs);
        pager = (ViewPager) root.findViewById(R.id.pager);
     //   adapter = new ContactPagerAdapter(getActivity());

    //    pager.setAdapter(adapter);

      //  tabs.setViewPager(pager);

        return root;
    }



    @SuppressWarnings("deprecation")
    @Override
    public void onStart() {
        super.onStart();

        // change dialog width
        if (getDialog() != null) {



                Display display = getActivity().getWindowManager().getDefaultDisplay();
               int fullWidth = display.getWidth();


            final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                    .getDisplayMetrics());

            int w = fullWidth - padding;
            int h = getDialog().getWindow().getAttributes().height;

            getDialog().getWindow().setLayout(w, h);
        }
    }


}
