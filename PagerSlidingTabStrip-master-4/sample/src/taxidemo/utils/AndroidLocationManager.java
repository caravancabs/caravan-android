package taxidemo.utils;

/**
 * Created by Rj on 2/13/2015.
 */

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class AndroidLocationManager implements LocationListener {

    private Context context;

    // flag for GPS
    boolean isGPSEnabled = false;

    // flag for network
    boolean isNetworkEnabled = false;

    Location location;
    double longitude;
    double latitude;


    // Minimum interval for update location in milliseconds
    private static long MIN_UPDATE_INTERVAL = 1000 * 60 * 1;

    // Minimum distance for updation of location in meters
    private static long MIN_UPDATE_DISTANCE = 5;

    LocationManager locationManager;

    public AndroidLocationManager(Context context) {
        // Get Context
        this.context = context;
        //getLocation();
    }

    public Location getLocation() {


        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // get GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // get network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isGPSEnabled || isNetworkEnabled)
        {
            // First get location from network

            if(isNetworkEnabled)
            {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_UPDATE_INTERVAL,
                            MIN_UPDATE_DISTANCE,
                            this);
                    Log.e("MYTAG", "Network");

                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            longitude = location.getLongitude();
                            latitude = location.getLatitude();
                            Log.e("MYTAG", "Network location : " + latitude + "," + longitude);
                        }
                    }

            }

            // If GPS is enabled get precise location from GPS
            if(isGPSEnabled)
            {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_UPDATE_INTERVAL,
                        MIN_UPDATE_DISTANCE,
                        this);
                Log.e("MYTAG", "GPS working");

                if(locationManager != null)
                {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if(location!=null)
                    {
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();
                        Log.e("MYTAG", "GPS location : "+latitude+","+longitude);
                    }
                    else
                        Log.e("MYTAG", "GPS location : location null");
                }
                else
                    Log.e("MYTAG", "GPS location : location manager null");
            }
        }
        else
        {

        }

        return location;
    }

	/*
	 *  Function to get Longitude
	 *  @return l
	 */

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }


    /*
     *  Callback functions for location change
     *
     */
    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

}
