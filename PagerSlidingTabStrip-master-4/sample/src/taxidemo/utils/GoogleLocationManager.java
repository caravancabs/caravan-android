package taxidemo.utils;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Rj on 2/21/2015.
 */
public class GoogleLocationManager {

    Context context;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    public GoogleLocationManager(Context context) {
        this.context = context;
        // First we need to check availability of play services
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                //.addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //GooglePlayServicesUtil.getErrorDialog(resultCode, context,
                  //      PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(context.getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }


    /**
     * Method to display the location on UI
     * */
    public LatLng getLocationLatLng(){



        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);



        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            return new LatLng(latitude, longitude);

        } else {
            Log.e(this.getClass().getSimpleName(), "last Location Latlng is null");
            return null;
        }
    }

}
