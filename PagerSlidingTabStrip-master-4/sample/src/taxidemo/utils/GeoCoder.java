package taxidemo.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rj on 2/19/2015.
 */
public class GeoCoder {


    public static String getAddress(Context context, LatLng location) throws IOException {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault()); //      Geocoder to convert addresses into coordinates and vice versa

        List<Address> address = null;                // to Store Address

        address = geocoder.getFromLocation(location.latitude, location.longitude, 1);

        String street = address.get(0).getAddressLine(0);
        String city =address.get(0).getAddressLine(1);
        String country = address.get(0).getAddressLine(2);

        return street+" "+city+" "+country;
    }




    public static LatLng getLocation(Context context, String address) throws IOException {

        Geocoder geocoder; //      Geocoder to convert addresses into coordinates and vice versa

        List<Address> addresses = null;                // to Store Address

        geocoder = new Geocoder(context, Locale.getDefault());

        addresses = geocoder.getFromLocationName(address, 1);//Returns an array of Addresses that are known to describe the named location

        LatLng loc = new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude());

        return loc;
    }
}