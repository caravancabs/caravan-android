package taxidemo;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import taxidemo.db.model.TaxiPrice;
import taxidemo.db.model.TaxiTime;
import taxidemo.mainview.TaxiAdapter;


public class TaxiApp extends Application{

    public static double duration;
    private static Context context;
    public static boolean refresh=true;
    private static List<TaxiTime> taxiTimes = new ArrayList<>();
    public boolean isEtaAdapter,isPriceAdapter;

    private static List<TaxiPrice> taxiPrices = new ArrayList<>();

    Location userLocation;

    public static String destAddress;
    Location srcLocation;
    Location destLocation;
    public static TaxiAdapter uberTaxiAdapter,uberAdapter, sureAdapter,priceAdapter,etaAdapter,meruAdapter,olaAdapter,sedanAdapter,hatchAdapter,primeAdapter;


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();


        uberTaxiAdapter=new TaxiAdapter(context,"");
        sureAdapter=new TaxiAdapter(getApplicationContext(),"Sure");
        uberAdapter=new TaxiAdapter(getApplicationContext(),"Uber");
        meruAdapter=new TaxiAdapter(getApplicationContext(),"Meru");
        olaAdapter=new TaxiAdapter(getApplicationContext(),"Ola");

        sedanAdapter=new TaxiAdapter(getApplicationContext(),"sedan");
        hatchAdapter=new TaxiAdapter(getApplicationContext(),"hatch");
        primeAdapter=new TaxiAdapter(getApplicationContext(),"prime");

        Log.e("tag","Application created");
        isEtaAdapter=isPriceAdapter=true;


    }

    public static Context getContext()
    {
        return context;
    }

    public  Location getUserLocation()
    {
        return userLocation;
    }

    public void setUserLocation(Location location)
    {
        userLocation=location;
    }
    public static List<TaxiTime> gettaxiTimes(String taxi)
    {


        if(taxi!=""){




        List<TaxiTime> taxiTime=new ArrayList<>();
        int j=0;


            if(taxi.contains("hatch"))
            {
                int z=taxiTimes.size();
                for(int i=0;i<z;i++) {
                    if (taxiTimes.get(i).getModelName().toLowerCase().contains("x")||taxiTimes.get(i).getModelName().toLowerCase().contains("go")||taxiTimes.get(i).getModelName().toLowerCase().contains("genie")||taxiTimes.get(i).getModelName().toLowerCase().contains("hatch")|| taxiTimes.get(i).getModelName().toLowerCase().contains("mini")) {
                        taxiTime.add(j, taxiTimes.get(i));
                        j++;
                    }}
            }
            else  if(taxi.contains("sedan"))
            {
                int z=taxiTimes.size();
                for(int i=0;i<z;i++) {
                    if (taxiTimes.get(i).getModelName().toLowerCase().contains("sedan")||taxiTimes.get(i).getModelName().toLowerCase().contains("meru")) {
                        taxiTime.add(j, taxiTimes.get(i));
                        j++;
                    }}
            }
            else  if(taxi.contains("prime"))
            {
                int z=taxiTimes.size();
                for(int i=0;i<z;i++) {
                    if (taxiTimes.get(i).getModelName().toLowerCase().contains("black")||taxiTimes.get(i).getModelName().toLowerCase().contains("prime")||taxiTimes.get(i).getModelName().toLowerCase().contains("meru")) {
                        taxiTime.add(j, taxiTimes.get(i));
                        j++;
                    }}
            }
            else {
                for (int i = 0; i < taxiTimes.size(); i++) {
                    if (taxiTimes.get(i).getCompanyName().contains(taxi)) {
                        taxiTime.add(j, taxiTimes.get(i));
                        j++;
                    }
                }

            }
          return taxiTime;}
else
        {


            return taxiTimes;

        }

    }

    public static void putTaxiTimeInList(TaxiTime taxi) {
        taxiTimes.add(taxi);
    }

    public static TaxiPrice getTaxiPriceByProductId(String productId)
    {
        for (int i=0;i<taxiPrices.size();i++)

        {
            if(productId.equals(taxiPrices.get(i).getProductId())){
                return taxiPrices.get(i);

            }
        }
        return null;
    }

    public static List<TaxiPrice> gettaxiPrices() {


        return taxiPrices;
    }

    public static void putTaxiPriceInList(TaxiPrice taxi) {
        taxiPrices.add(taxi);
        Log.d("Price le"," added");
    }

    public void setSrcLocation(Location src)
    {
        srcLocation=src;

    }
    public Location getSrcLocation(){
        return srcLocation;
    }

    public void setDestLocation(Location dest)
    {
        destLocation=dest;
    }
    public Location getDestLocation()
    {
        return destLocation;
    }

    public static void setDistance(double dest)
    {
        duration=dest;
    }
    public static double getDistance()
    {
        return duration;
    }


    public TaxiAdapter getUberTaxiAdapter()
{

    return uberTaxiAdapter;
}
    public TaxiAdapter getSureAdapter()
    {
        return sureAdapter;
    }

    public void setRefresh(){refresh=false;}

}
