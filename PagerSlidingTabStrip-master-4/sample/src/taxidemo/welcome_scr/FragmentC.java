package taxidemo.welcome_scr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.caravan.app.R;


public class FragmentC extends Fragment {

	Button bt_wel_letsgo;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_c, container, false);

		bt_wel_letsgo = (Button) view.findViewById(R.id.bt_wel_letsgo);

		bt_wel_letsgo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				SharedPreferences sp = getActivity().getSharedPreferences("user_info", 0);
				//int ch = sp.getInt("is_loged_in", 0);
				SharedPreferences.Editor ed = sp.edit();
				ed.putInt("is_loged_in", 1);
				ed.commit();

				Toast.makeText(getActivity(),"LetsGo...!",Toast.LENGTH_SHORT).show();

				startActivity(new Intent(getActivity(), taxidemo.mainview.Homepage.class));
				getActivity().finish();
			}
		});

		return view;
	}
}
