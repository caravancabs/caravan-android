package taxidemo.welcome_scr;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caravan.app.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import me.relax.circleindicator.CircleIndicator;

public class Welcome extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        SharedPreferences sp = getSharedPreferences("user_info", MODE_PRIVATE);
        int ch = sp.getInt("is_loged_in", 0);
        //if(ch!=1){
        if(false){
        }else{
            startActivity(new Intent(Welcome.this,taxidemo.mainview.Homepage.class));
            finish();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        CircleIndicator defaultIndicator = (CircleIndicator) findViewById(R.id.indicator_welcome);
        ViewPager parallaxViewPager = (ViewPager) findViewById(R.id.parallaxviewpager);
        parallaxViewPager.setAdapter(new WelcomeAdapter(fragmentManager));
        defaultIndicator.setViewPager(parallaxViewPager);

        defaultIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override public void onPageScrolled(int i, float v, int i2) {

                TextView tv_info = (TextView) findViewById(R.id.tv_wel_info);
                TextView frag2 = (TextView) findViewById(R.id.textView_frag2);
                try {
                    YoYo.with(Techniques.FadeInUp)
                            .playOn(tv_info);
                    YoYo.with(Techniques.FadeInUp)
                            .playOn(frag2);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override public void onPageSelected(int i) {
                Log.d("OnPageChangeListener", "Current selected = " + i);
                TextView tv_info = (TextView) findViewById(R.id.tv_wel_info);
                try {
                    YoYo.with(Techniques.FadeInUp)
                            .playOn(tv_info);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override public void onPageScrollStateChanged(int i) {

            }
        });

        parallaxViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {

            @SuppressLint("NewApi")
            @Override
            public void transformPage(View view, float position) {
                int pageWidth = view.getWidth();

                ImageView iv1f1,iv1f2,iv2f2,iv3f2;//,iv5,iv6,iv7;
                iv1f1 = (ImageView) findViewById(R.id.imageView_car);
                iv1f2 = (ImageView) findViewById(R.id.imageView_car1);
                iv2f2 = (ImageView) findViewById(R.id.imageView_car2);
                iv3f2 = (ImageView) findViewById(R.id.imageView_car3);

                try {
                    //ImageView img = (ImageView) findViewById(R.id.imgadobe);
                    //TextView tv1 = (TextView) findViewById(R.id.textView1);
                    if (position < -1) { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        view.setAlpha(0);

                    } else if (position <= 1) { // [-1,1]

                        iv1f1.setTranslationX((float) (-(1 - position) * 1.5 * pageWidth));

                        //iv2.setTranslationX((float) (-(1 - position) * pageWidth));

                        iv1f2.setTranslationX((position) * (pageWidth / 1));
                        // iv1f1.setTranslationX(-(position) * (pageWidth / 2));
                        iv2f2.setTranslationX((position) * (pageWidth / 4));
                        iv3f2.setTranslationX((position) * (pageWidth / 2));
                        //iv2.setTranslationX(-(position) * (pageWidth / 2));
                        //iv3.setTranslationX((position) * (pageWidth / 4));
                        //iv5.setTranslationX(-(position) * (pageWidth / 4));


                    } else { // (1,+Infinity]
                        view.setAlpha(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}

class WelcomeAdapter extends FragmentPagerAdapter {

    public WelcomeAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int arg0) {
        Fragment fragment = null;

        if(arg0==0){
            fragment = new FragmentA();
        }if(arg0==1){
            fragment = new FragmentB();
        }if(arg0==2){
            fragment = new FragmentC();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

}

