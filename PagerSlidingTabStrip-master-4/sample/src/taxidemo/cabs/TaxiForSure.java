package taxidemo.cabs;

import android.location.Location;
import android.util.Log;


import com.caravan.app.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import taxidemo.TaxiApp;
import taxidemo.db.model.TaxiTime;


public class TaxiForSure  {

    private Location location;
    private static final String BASE_URL="http://iospush.taxiforsure.com/getNearestDriversForApp/";
    private static final String CITY="pune";
    private static final String APP_VERSION="4.1.1";

    public TaxiForSure(Location location)
    {
        this.location=location;
    }

    public void getTaxiForSure(List<TaxiTime> list)
    {


        /*      EXAMPLE

http://iospush.taxiforsure.com/getNearestDriversForApp/?
longitude=" + paramString2 + "&latitude=" + paramString1 + "&density=320&appVersion=4.1.1
         */


        List<NameValuePair> paramList = new ArrayList<>();
        paramList.add(new BasicNameValuePair("longitude", String.valueOf(location.getLongitude())));
        paramList.add(new BasicNameValuePair("latitude", String.valueOf(location.getLatitude())));
        paramList.add(new BasicNameValuePair("density", "820"));
        paramList.add(new BasicNameValuePair("appVersion",APP_VERSION));

        // Encode params in base url and get a complete, directly usable url
        String paramString = URLEncodedUtils.format(paramList, HTTP.UTF_8);
        String requestUrl = BASE_URL + "?" + paramString;

        String jsonString = getJsonStringFromURL(requestUrl);

        try {
            // the root object we get from url
            JSONObject rootObject = new JSONObject(jsonString);
            JSONObject responseData=rootObject.getJSONObject("response_data");

            JSONArray taxiArray = responseData.getJSONArray("data");



            // Get Taxi item and put it in list

            /*
            TAXIFORSURE FIELDS

             "distance": "5.19",
        "uuid": "8f7cae87-356c-44a8-80f8-86603e7d5ef9",
        "Distance": "5.19",
        "carType": "Eeco",
        "longitude": "73.8108429",
        "duration": 32,
        "Time": 1423725496829,
        "latitude": "18.4852298",
        "city": "Pune"

             */
            boolean hatch,sedan,echo;
            hatch=sedan=echo=false;
            TaxiTime tSdan = null,tHatch = null,tEcco = null;
            for (int i = 0; i < taxiArray.length(); i++)
            {
                TaxiTime taxi=parseJsonObjectToTaxiData(taxiArray.getJSONObject(i));
                if(taxi!=null){
                    if(taxi.getModelName().contains("Sedan"))
                    {
                        if(sedan==false){
                        tSdan=taxi;
                        sedan=true;}
                        else
                        {
                            if(taxi.getTimeInSec()<tSdan.getTimeInSec())
                            {
                                tSdan=taxi;
                            }
                        }
                    }
                 else   if(taxi.getModelName().contains("Hatch"))
                    {
                        if(hatch==false){
                            tHatch=taxi;
                            hatch=true;}
                        else
                        {
                            if(taxi.getTimeInSec()<tHatch.getTimeInSec())
                            {
                                tHatch=taxi;
                            }
                    }}
                    else if(taxi.getModelName().contains("Eeco"))
                    {
                        if(echo==false){
                            tEcco=taxi;
                            echo=true;}
                        else
                        {
                            if(taxi.getTimeInSec()<tEcco.getTimeInSec())
                            {
                                tEcco=taxi;
                            }}
                    }

                }
            }
            if(tHatch!=null)
            {
                list.add(tHatch);
                TaxiApp.sureAdapter.add(tHatch);
                TaxiApp.hatchAdapter.add(tHatch);

            }
            if(tSdan!=null)
            {
                list.add(tSdan);
            TaxiApp.sureAdapter.add(tSdan);
                TaxiApp.sedanAdapter.add(tSdan);
            }
            if (tEcco!=null)
            {
                list.add(tEcco);
                TaxiApp.sureAdapter.add(tEcco);
                TaxiApp.hatchAdapter.add(tEcco);
            }






        } catch (JSONException e) {
            Log.e("tag", "JSON Error.....");
            e.printStackTrace();
        }
    }

    private String getJsonStringFromURL(String requestUrl)
    {

        // Buffered Reader to read response line by line
        BufferedReader reader = null;

        URL url;
        try
        {
            // Create URL object
            url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            StringBuilder builder = new StringBuilder();

            if(conn.getInputStream()!=null)
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            else
                Log.d("reader","Empty");
            String line;
            while((line = reader.readLine())!= null)
            {
                builder.append(line + "\n");
            }

            // Console

            Log.e("tag", "GOT Taxiforsure : \n\n"+ builder.toString());

            return builder.toString();

        } catch (MalformedURLException e)
        {
            Log.d("Error","Mulfunction");
            return null;
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        finally{
            if(reader != null)
            {
                try{
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

    }


    private TaxiTime parseJsonObjectToTaxiData(JSONObject jsonObject) {

        try {
            String taxiName=jsonObject.getString("carType");
            int timeInMin=jsonObject.getInt("duration");
         //   String distance=jsonObject.getString("distance");
            String productId=jsonObject.getString("uuid");

            TaxiTime taxi = new TaxiTime(productId, "TaxiForSure", taxiName, timeInMin*60);
            taxi.setDrawable(R.drawable.taxiforsure);
            return taxi;
        } catch (JSONException e) {
            Log.e("tag", "parseJson : one cannot parse");
            e.printStackTrace();
            return null;
        }
    }


}
