package taxidemo.cabs;


import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;


import com.caravan.app.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import taxidemo.TaxiApp;
import taxidemo.db.model.TaxiTime;
import taxidemo.mainview.TaxiAdapter;


public class Ola
{
    JSONObject jsonobject;
    TaxiAdapter adapter;

    protected Context context;
    Location location;
    public Ola(Context context,Location location)
    {
        this.context=context;
        this.location=location;
    }

    public void olaAccess( TaxiAdapter adapter1)
    {
        //  Toast.makeText(context,"Ola Ola",Toast.LENGTH_SHORT).show();
        this.adapter=adapter1;
        String lattitude=String.valueOf(location.getLatitude());
        String longitude=String.valueOf(location.getLongitude());

        OlaAsynTask olaAsynTask = new OlaAsynTask();
        olaAsynTask.execute(lattitude, longitude);

  /*      StringRequest stringrequest = new StringRequest((new StringBuilder("http://mapi.olacabs.com/v3/cab/info?custom_lat=")).append(lattitude).append("&custom_lng=").append(longitude).append("&accuracy=10.0&fix_time=1414867768827&speed=0.0&altitude=0.0&location_type=CUSTOM&selected_by=USER&cab_category=economy_sedan&user_id=yoT7Qnhav4qJ10Xccpz3fgyl1YIjM95z2PbwxuZ%2BzJWludCHj5sXgqBtYWFh%0AxWxydqH7fCQVOGSexP%2BtV5KfrA%3D%3D%0A++++&enable_auto=true&enable_delivery=true").toString(), new com.android.volley.Response.Listener() {

//            StringRequest stringrequest = new StringRequest((new StringB("http://mapi.olacabs.com/v3/cab/info?custom_lat=")).append(s).append("&custom_lng=").append(s1).append("                &accuracy=10.0&fix_time=1414867768827&speed=0.0&altitude=0.0&location_type=CUSTOM&selected_by=USER&cab_category=economy_sedan&user_id=z%2FSOjdAeOaXpiu3YGpZbX4yan5NjzSyoFQ3bMhayj%2FLK11hcaHYppAIW6bxl%0AU%2BVT64RfuNjRmCF8em1tgbyD3g%3D%3D%0A++++&enable_auto=true&enable_delivery=true").toString()

            public  void onResponse(Object obj)
            {
                try {
                    onResponse((String)obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onResponse(String s2) throws JSONException {

                VolleyLog.v("Response:%n %s", new Object[] {
                        s2
                });
                Log.d("onResponse", s2);
                HashMap hashmap;

                try
                {
                    jsonobject = new JSONObject(s2);

                    hashmap = new HashMap();

                    JSONArray   jsonarray1 = jsonobject.optJSONArray("cab_categories");
                    List<TaxiTime> list= TaxiApp.gettaxiTimes("");
                    for(int i=0;i<jsonarray1.length();i++)
                    {
                        TaxiTime taxi=parseJsonObjectToTaxiData(jsonarray1.getJSONObject(i));
                        if(taxi!=null){
                            list.add(taxi);
                            TaxiApp.olaAdapter.add(taxi);
                            String model=taxi.getModelName();
                            if(model.contains("ini"))
                                TaxiApp.hatchAdapter.add(taxi);
                            else if(model.contains("Sedan"))
                                TaxiApp.sedanAdapter.add(taxi);
                            else if(model.contains("Prime"))
                                TaxiApp.primeAdapter.add(taxi);

                        }
                        else
                            Log.e("ola","taxi null");
                    }
                    adapter.notifyDataSetChanged();


                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    return;
                }

                JSONArray jsonarray1;
                ArrayList arraylist;

                arraylist = new ArrayList();
          }



        }, new com.android.volley.Response.ErrorListener() {


            public void onErrorResponse(VolleyError volleyerror)
            {
                Log.d("Error","Found"+volleyerror.toString())  ;          }



        }) {


            public Map getHeaders()
                    throws AuthFailureError
            {
                Log.d("getting","header");
                HashMap hashmap = new HashMap();
                hashmap.put("api-key", "@ndro1d");
                hashmap.put("Host", "mapi.olacabs.com");
                hashmap.put("client", "android");
                return hashmap;
            }


        };
        RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());
        queue.add(stringrequest);
        //return jsonobject;*/

    }



    private TaxiTime parseJsonObjectToTaxiData(JSONObject jsonObject) {

        try {
            //String taxiName=jsonObject.getString("cabServiceDisplayName");
            String timeInMin=jsonObject.getString("estimateTime");
            String modelName = jsonObject.getString("displayName");
            //  String distance=jsonObject.getString("distance");
            String productId="Ola ("+modelName+")";
//         int start=timeInMin.indexOf(":");
            //           int end=timeInMin.indexOf(",");


            int time = Integer.parseInt(timeInMin);//timeInMin.substring(start+1,end));
            TaxiTime taxi = new TaxiTime(productId, "Ola", modelName, time*60);
            taxi.setDrawable(R.drawable.ola);
            return taxi;
        } catch (JSONException e) {
            Log.e("OLA", "parseJson : one cannot parse");
            e.printStackTrace();
            return null;
        }
    }


    class OlaAsynTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(String... params) {
            String url = null;
            url = "http://dev-api.caravancabs.com:8585/getLocation";//?lat=18.457954&lng=73.8509909&sortby=price";
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("lat", params[0]));
                nameValuePairs.add(new BasicNameValuePair("lng", params[1]));
                nameValuePairs.add(new BasicNameValuePair("sortby", "time"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String bufferedStrChunk = null;
                while((bufferedStrChunk = bufferedReader.readLine()) != null){
                    stringBuilder.append(bufferedStrChunk);
                }
                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("OLA__OLA", result);
            ArrayList<JSONObject> listJObj = new ArrayList<JSONObject>();
            // Toast.makeText(context, ""+result,Toast.LENGTH_SHORT).show();
            try {
                JSONObject jObj = new JSONObject(result);
                JSONArray jObj1 = jObj.getJSONArray("data");
                Log.d("o_l_a","Whole Array: "+jObj1.toString());
                for (int i=0; i<jObj1.length();i++){
                    JSONObject j = (JSONObject) jObj1.getJSONObject(i);
                    Log.d("o_l_a","loop1 JSONOBJ"+i+" : "+j.toString());
                    String cabS = j.getString("cabService");
                    Log.d("o_l_a","cabService:  "+cabS);
                    if(cabS.equals("Ola")){
                        listJObj.add(j);
                        Log.d("o_l_a", "List::" + listJObj.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ////
            List<TaxiTime> list= TaxiApp.gettaxiTimes("");
            for(int i=0;i<listJObj.size();i++)
            {
                TaxiTime taxi=parseJsonObjectToTaxiData(listJObj.get(i));
                if(taxi!=null){
                    list.add(taxi);
                    TaxiApp.olaAdapter.add(taxi);
                    String model=taxi.getModelName();
                    if(model.contains("ini"))
                        TaxiApp.hatchAdapter.add(taxi);
                    else if(model.contains("Sedan"))
                        TaxiApp.sedanAdapter.add(taxi);
                    else if(model.contains("Prime"))
                        TaxiApp.primeAdapter.add(taxi);

                }
                else
                    Log.e("ola","taxi null");
            }
            adapter.notifyDataSetChanged();

            ////

            super.onPostExecute(result);
        }
    }


}
