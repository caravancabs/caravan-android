package taxidemo.cabs;

import android.location.Location;
import android.util.Log;


import com.caravan.app.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import taxidemo.TaxiApp;
import taxidemo.db.model.TaxiPrice;
import taxidemo.db.model.TaxiTime;


public class Uber {
//      TAXI API DATA MEMBERS
    private static final String BASE_URL = "https://api.uber.com/v1/estimates";
    private static final String TIME = "time";
    private static final String TIMES = "times";
    private static final String PRICE = "price";
    private static final String PRICES = "prices";

    private static final String KEY_SERVER_TOKEN = "server_token";
    private static final String KEY_START_LAT = "start_latitude";
    private static final String KEY_START_LNG = "start_longitude";
    private static final String KEY_END_LAT = "end_latitude";
    private static final String KEY_END_LNG = "end_longitude";

    private static final String SERVER_TOKEN = "23R0cVjywyoQvidQKQ2QS-UMnI2DEIZlnb6vLUGy";



    private Location location;  //USER LOCATION

    public Uber(Location location) {
        this.location = location;
    }

    /*
            This method will get taxi info from HTTP URL and put it into the list
            And then the adapter should be notified for data change
            thus, a adapter.notifyDataSetChanged() should be called after calling this method

            ** This is parent method, other methods will be helper methods
         */
    public void putTaxiTimeInList(List<TaxiTime> list)
    {

        // Put the parameters in List
        List<NameValuePair> paramList = new ArrayList<>();
        paramList.add(new BasicNameValuePair(KEY_SERVER_TOKEN, SERVER_TOKEN));
        paramList.add(new BasicNameValuePair(KEY_START_LAT, String.valueOf(location.getLatitude())));
        paramList.add(new BasicNameValuePair(KEY_START_LNG, String.valueOf(location.getLongitude())));

        // Encode params in base url and get a complete, directly usable url
        String paramString = URLEncodedUtils.format(paramList, HTTP.UTF_8);
        String requestUrl = BASE_URL +"/" + TIME + "?" + paramString;

        String jsonString = getJsonStringFromURL(requestUrl);

        try {
            // the root object we get from url
            JSONObject rootObject = new JSONObject(jsonString);

            // Get array of taxies and times from rootObject
            JSONArray taxiArray = rootObject.getJSONArray(TIMES);

            // Get Taxi item and put it in list
            for (int i = 0; i < taxiArray.length(); i++) {
           TaxiTime taxi = parseJsonObjectToTaxiTime(taxiArray.getJSONObject(i));
                if(taxi!=null)
                {
                    list.add(taxi);
                    TaxiApp.uberAdapter.add(taxi);
                    String model=taxi.getModelName();
                    if(model.contains("uberX") || model.contains("uberGO"))
                        TaxiApp.hatchAdapter.add(taxi);
                    else
                        TaxiApp.primeAdapter.add(taxi);
                }

            }

        } catch (JSONException e) {
            Log.e("Uber", "JSON Error.....");
            e.printStackTrace();
        }
    } /*
            This method will get price from another API call

            ** This is parent method, other methods will be helper methods
         */
    public void putTaxiPriceInList(List<TaxiPrice> list, Double srcLat,Double srcLong,Double destLat,Double destLong)
    {

        // Put the parameters in List
        List<NameValuePair> paramList = new ArrayList<>();
        paramList.add(new BasicNameValuePair(KEY_SERVER_TOKEN, SERVER_TOKEN));
        paramList.add(new BasicNameValuePair(KEY_START_LAT, String.valueOf(srcLat)));
        paramList.add(new BasicNameValuePair(KEY_START_LNG, String.valueOf(srcLong)));
        paramList.add(new BasicNameValuePair(KEY_END_LAT, String.valueOf(destLat)));
        paramList.add(new BasicNameValuePair(KEY_END_LNG, String.valueOf(destLong)));

        // Encode params in base url and get a complete, directly usable url
        String paramString = URLEncodedUtils.format(paramList, HTTP.UTF_8);
        String requestUrl = BASE_URL +"/" + PRICE + "?" + paramString;

        String jsonString = getJsonStringFromURL(requestUrl);

        try {
            // the root object we get from url
            JSONObject rootObject = new JSONObject(jsonString);

            // Get array of taxies and times from rootObject
            JSONArray taxiArray = rootObject.getJSONArray(PRICES);

            // Get Taxi item and put it in list
            for (int i = 0; i < taxiArray.length(); i++) {
                TaxiPrice taxi = parseJsonObjectToTaxiPrice(taxiArray.getJSONObject(i));
                if(taxi!=null)
                    list.add(taxi);
                Log.d("Uber","Put price in list");
            }

        } catch (JSONException e) {
            Log.e("Uber", "JSON Error..." + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private TaxiPrice parseJsonObjectToTaxiPrice(JSONObject jsonObject) {

        try {
            String productId = jsonObject.getString("product_id");
            Double distance = jsonObject.getDouble("distance");
            Integer duration = jsonObject.getInt("duration");
            TaxiApp.setDistance(distance);
            String displayName = jsonObject.getString("display_name");
            int surgeMulti = jsonObject.getInt("surge_multiplier");
            double highEstimate = jsonObject.getInt("high_estimate");
            double lowEstimate = jsonObject.getInt("low_estimate");
            String currencyCode = jsonObject.getString("currency_code");
            String estimateAvg = jsonObject.getString("estimate");
            TaxiPrice taxi = new TaxiPrice(productId, distance, duration, displayName, surgeMulti, highEstimate, lowEstimate, currencyCode, estimateAvg);
            return taxi;
        } catch (JSONException e) {
            Log.e("Uber", "parseJson : one cannot parse");
            e.printStackTrace();
            return null;
        }
    }


    private String getJsonStringFromURL(String requestUrl)
    {

        // Buffered Reader to read response line by line
        BufferedReader reader = null;

        URL url;
        try
        {
            // Create URL object
            url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            StringBuilder builder = new StringBuilder();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            while((line = reader.readLine())!= null)
            {
                builder.append(line + "\n");
            }

            // Console
            Log.e("Uber", "GOT : \n\n"+ builder.toString());

            return builder.toString();

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        finally{
            if(reader != null)
            {
                try{
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

    }

    private TaxiTime parseJsonObjectToTaxiTime(JSONObject object)
    {
        try {
            String companyName = "Uber Cabs";
            String productId = object.getString("product_id");
            String modelName = object.getString("display_name");
            Integer time = object.getInt("estimate");
            TaxiTime taxi = new TaxiTime(productId, companyName, modelName, time);
            taxi.setDrawable(R.drawable.uber);
            return taxi;
        } catch (JSONException e) {
            Log.e("Uber", "parseJson : one cannot parse");
            e.printStackTrace();
            return null;
        }
    }


    public void getPrice( Double srcLat,Double srclong,Double destLat,Double destLong)
    {

        Log.d("getting","price");
        // Put the parameters in List
        List<NameValuePair> paramList = new ArrayList<>();
        paramList.add(new BasicNameValuePair(KEY_SERVER_TOKEN, SERVER_TOKEN));
        paramList.add(new BasicNameValuePair(KEY_START_LAT, String.valueOf(srcLat)));
        paramList.add(new BasicNameValuePair(KEY_START_LNG, String.valueOf(srclong)));
        paramList.add(new BasicNameValuePair(KEY_END_LAT, String.valueOf(destLat)));
        paramList.add(new BasicNameValuePair(KEY_END_LNG, String.valueOf(destLong)));

        // Encode params in base url and get a complete, directly usable url
        String paramString = URLEncodedUtils.format(paramList, HTTP.UTF_8);
        String requestUrl = BASE_URL +"/" + PRICE + "?" + paramString;

        String jsonString = getJsonStringFromURL(requestUrl);

        try {
            // the root object we get from url
            JSONObject rootObject = new JSONObject(jsonString);

            // Get array of taxies and times from rootObject
            JSONArray taxiArray = rootObject.getJSONArray(PRICES);


            ArrayList<HashMap<String,String>> PriceList=new ArrayList<HashMap<String, String>>();
            // Get Taxi item and put it in list
            for (int i = 0; i < taxiArray.length(); i++) {

                JSONObject c=taxiArray.getJSONObject(i);

                Log.d("JSON: ",c.toString());


            }

        } catch (JSONException e) {
            Log.e("Uber", "JSON Error..." + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }



}


/*
{
	"times":[
		{
			"product_id":"a1111c8c-c720-46c3-8534-2fcdd730040d",
			"display_name":"uberX",
			"estimate":253,
			"localized_display_name":"uberX"
		},
		{
			"product_id":"821415d8-3bd5-4e27-9604-194e4359a449",
			"display_name":"uberXL",
			"estimate":349,
			"localized_display_name":"uberXL"
		}
	]
}
 */

/*
                params2.add(new BasicNameValuePair("server_token", "23R0cVjywyoQvidQKQ2QS-UMnI2DEIZlnb6vLUGy"));
                params2.add(new BasicNameValuePair("start_latitude", "37.775818"));
                params2.add(new BasicNameValuePair("start_longitude", "-122.418028"));
                params2.add(new BasicNameValuePair("end_latitude", "38.83279"));
                params2.add(new BasicNameValuePair("end_longitude", "-122.418228"));
                */