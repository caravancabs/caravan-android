package taxidemo.cabs;

import android.location.Location;
import android.util.Log;


import com.caravan.app.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import taxidemo.TaxiApp;
import taxidemo.db.model.TaxiTime;


public class Meru  {

    private   Location location;
    boolean isMeru=false;
    boolean isGenie=false;
    boolean isEve=false;
    int rand1,rand2;
    private static final String BASE_URL="http://mobileapp.merucabs.com/NearByCab_ETA/GetNearByCabs.svc/rest/nearby";
    int mobile=8897;
  public Meru(Location location)
  {
      this.location=location;
      Random random=new Random();
      rand1=random.nextInt(1000);
      rand2=random.nextInt(1000);
  }
    public void putTaxiTimeInList(List<TaxiTime> list)
    {


        /*      EXAMPLE

http://mobileapp.merucabs.com/NearByCab_ETA/GetNearByCabs.svc/rest/nearby?
Lat=18.458147&Lng=73.849568&SuggestedRadiusMeters=5000&CabMaxCount=10

         */


        List<NameValuePair> paramList = new ArrayList<>();
        paramList.add(new BasicNameValuePair("Lat", String.valueOf(location.getLatitude())));
        paramList.add(new BasicNameValuePair("Lng", String.valueOf(location.getLongitude())));
        paramList.add(new BasicNameValuePair("SuggestedRadiusMeters","5000"));
        paramList.add(new BasicNameValuePair("CabMaxCount","10"));
        paramList.add(new BasicNameValuePair("DeviceId","30120C73F241DAE"));
        paramList.add(new BasicNameValuePair("DeviceType","Android"));
        paramList.add(new BasicNameValuePair("Channel","Mobile"));
        paramList.add(new BasicNameValuePair("Brand","Meru|Genie"));
        paramList.add(new BasicNameValuePair("Mobile",Integer.toString(mobile)+Integer.toString(rand1)+Integer.toString(rand2)));
        // Encode params in base url and get a complete, directly usable url
        String paramString = URLEncodedUtils.format(paramList, HTTP.UTF_8);
        String requestUrl = BASE_URL + "?" + paramString;

        String jsonString = getJsonStringFromURL(requestUrl);

        String jsonData=jsonString.substring(68,jsonString.length()-10);
        Log.d("String",jsonData);
        try {
            // the root object we get from url
            JSONObject rootObject = new JSONObject(jsonData);

            // Get array of taxies and times from rootObject
            JSONArray taxiArray = rootObject.getJSONArray("Cablist");

            // Get Taxi item and put it in list
            for (int i = 0; i < taxiArray.length(); i++) {
                //Log.d("",taxiArray.getJSONObject(i).toString());
                TaxiTime taxi =  parseJsonObjectToTaxiTime(taxiArray.getJSONObject(i));
                if(taxi!=null)
                {
                    list.add(taxi);
                    TaxiApp.meruAdapter.add(taxi);
                    if(taxi.getModelName().contains("Meru"))
                        TaxiApp.sedanAdapter.add(taxi);
                    else
                        TaxiApp.hatchAdapter.add(taxi);
                }
            }

        } catch (JSONException e) {
            Log.e("tag", "JSON Error.....");
            e.printStackTrace();
        }
    }

    private String getJsonStringFromURL(String requestUrl)
    {

        // Buffered Reader to read response line by line
        BufferedReader reader = null;

        URL url;
        try
        {
            // Create URL object
            url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            StringBuilder builder = new StringBuilder();

            if(conn.getInputStream()!=null)
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            else
                Log.d("reader","Empty");
            String line;
            while((line = reader.readLine())!= null)
            {
                builder.append(line + "\n");
            }

            // Console
            Log.e("tag", "Meru : \n\n"+ builder.toString());

            return builder.toString();

        } catch (MalformedURLException e)
        {
            Log.d("Error","Mulfunction");
            return null;
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        finally{
            if(reader != null)
            {
                try{
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

    }
    private TaxiTime parseJsonObjectToTaxiTime(JSONObject object)
    {
        try {
            String companyName = "Meru Cabs";
            String productId = object.getString("DeviceId");
            String modelName = object.getString("Brand");
            Integer time = object.getInt("TrafficETA");
            Log.d(modelName,String.valueOf(time));
          //  Log.d(modelName,time.toString());


            if((isGenie==false && modelName.contains("Genie")) ||( isMeru==false && modelName.contains("Meru")) ||(modelName.contains("Eve") && isEve==false))
            {

                 TaxiTime taxi = new TaxiTime(productId, companyName, modelName, time*60);
                 taxi.setDrawable(R.drawable.meru);
                if(modelName.contains("Genie"))
                isGenie=true;
                else if(modelName.contains("Meru")) {
                    isMeru = true;
                }
                else if(modelName.contains("Eve"))
                {
                    isEve=false;
                }
                return taxi;
        } }catch (JSONException e) {
            Log.e("Meru", "parseJson : one cannot parse");
            e.printStackTrace();

        }
        return null;
    }


}
