package taxidemo.mainview;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.caravan.app.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import taxidemo.DetailView.DetailActivity;
import taxidemo.TaxiApp;
import taxidemo.cabs.Meru;
import taxidemo.cabs.Ola;
import taxidemo.cabs.TaxiForSure;
import taxidemo.cabs.Uber;
import taxidemo.utils.AndroidLocationManager;

public class MainActivity extends ActionBarActivity {

    private Location location,destLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    TaxiAdapter adapter;
    AndroidLocationManager manager;
    ProgressBar pb;
    Context context;
    int startActivity=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cab);
        // Locate ListView from xml and link
//        ListView listView = (ListView) findViewById(R.id.listView);

    context=this.getApplicationContext();
        // Set progressbar
        pb = (ProgressBar) findViewById(R.id.progb123);
        //pp.execute("");
      //  pb.setVisibility(View.INVISIBLE);
        // Custom Adapter for taxis initialised

  //      listView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
        // On click listner for list of taxis
        //listView.setOnItemClickListener(this);
        TaxiApp taxiapp = (TaxiApp) getApplicationContext();

        adapter=taxiapp.getUberTaxiAdapter();
        location = taxiapp.getSrcLocation();
        destLocation=taxiapp.getDestLocation();
        adapter.clear();


        getData();




        Log.e("Tag", "MainActivity.onCreate Done !!!!");
    }
/*
    @Override
    protected void onResume() {
        super.onResume();
        // Get Location from UserLoacationManager
        manager = new AndroidLocationManager(this);
        getDataFromUBER();

    }
*/
private void getDataFromUBER() {



    // get taxi list from Uber
    new AsyncTask<Location,Void,Boolean>()
    {
        @Override
        protected void onPreExecute() {
        //    Toast.makeText(getApplicationContext(),"pb",Toast.LENGTH_SHORT).show();
          //  findViewById(R.id.empty).setVisibility(View.GONE);
            Log.e("Uber", "Progressbar Visibility set");


        }

        @Override
        protected Boolean doInBackground(Location... params) {
            try {
                Uber uber = new Uber(params[0]);
                uber.putTaxiTimeInList(TaxiApp.gettaxiTimes(""));
                uber.putTaxiPriceInList(TaxiApp.gettaxiPrices(),location.getLatitude(),location.getLongitude(),destLocation.getLatitude(),destLocation.getLongitude());
                Log.e("Uber", "taxi put in list");
                return true;
            }catch (Exception e){
                Log.e("Uber", "No taxi added to list");
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(TaxiApp.gettaxiTimes("uber").size()==0)
             //   findViewById(R.id.empty).setVisibility(View.VISIBLE);
            if(result!=null && result==true)
                adapter.notifyDataSetChanged();
            else
            {
                Log.d("failed","uber");
                //    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

            }

            if((++startActivity)==4)
            {//akshay
                //startActivity(new Intent(MainActivity.this, com.astuetz.viewpager.extensions.caravan.MainActivity.class));
                //finish();

            }
        }
    }.execute(location);
}

    // Getting data from Meru
    private void getDataFromMeru() {

        // get taxi list from Meru
        new AsyncTask<Location,Void,Boolean>()
        {

            @Override
            protected Boolean doInBackground(Location... params) {
                try {
                    Meru meru=new Meru(params[0]);
                    meru.putTaxiTimeInList(TaxiApp.gettaxiTimes(""));
                    Log.e("", "taxi put in list Meru");
                    return true;
                }catch (Exception e){
                    Log.e("", "No taxi added to list Meru");
                    e.printStackTrace();
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean result) {
             //   pb.setVisibility(View.GONE);

                if(TaxiApp.gettaxiTimes("").size()==0)
               //     findViewById(R.id.empty).setVisibility(View.VISIBLE);
                if(result!=null && result==true){
                 //   findViewById(R.id.empty).setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();

                }
                else
                {
                    Log.d("failed","Meru");
                    //    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

                }
                if((++startActivity)==4)
                {//akshay
                    //startActivity(new Intent(MainActivity.this, com.astuetz.viewpager.extensions.caravan.MainActivity.class));
                    //finish();
                }
            }
        }.execute(location);
    }
//End data

    //Getting data from Ola

    private void getDataFromOla() {

        // get taxi list from Ola
        new AsyncTask<Location,Void,Boolean>()
        {

            @Override
            protected Boolean doInBackground(Location... params) {
                try {
                    Ola ola=new Ola(context,params[0]);
                    ola.olaAccess(adapter);
                    return true;
                }catch (Exception e){
                    Log.e("Tag", "No taxi added to list Ola");
                    e.printStackTrace();
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean result) {

                if(TaxiApp.gettaxiTimes("uber").size()==0)

        //            findViewById(R.id.empty).setVisibility(View.VISIBLE);
            //    if(result!=null && result==true)
          //          adapter.notifyDataSetChanged();

                {
                    Log.d("failed","Ola");
                    //    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();

                }
                if((++startActivity)==4)
                {//akshay
                    //startActivity(new Intent(MainActivity.this, com.astuetz.viewpager.extensions.caravan.MainActivity.class));
                    //finish();
                }
            }
        }.execute(location);
    }


    // Option menu


    // Getting data from taxiforsure
    private void getDataFromTaxiForSure() {

        // get taxi list from TaxiForSure
        new AsyncTask<Location,Void,Boolean>()
        {

            @Override
            protected Boolean doInBackground(Location... params) {
                try {
                    TaxiForSure taxiForSure=new TaxiForSure(params[0]);
                    taxiForSure.getTaxiForSure(TaxiApp.gettaxiTimes(""));
                    Log.e("Tag", "taxi put in list TaxiForSure");
                    return true;
                }catch (Exception e){
                    Log.e("Tag", "No taxi added to list TaxiForSure");
                    e.printStackTrace();
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean result) {
                if(TaxiApp.gettaxiTimes("taxiforsure").size()==0)
 //                   findViewById(R.id.empty).setVisibility(View.VISIBLE);
                if(result!=null && result==true)
                    adapter.notifyDataSetChanged();
                else
                {
                    Log.d("failed","Taxiforsure");
                    //    Toast.makeText(MainActivity.this, "Failed to Load data for Uber",Toast.LENGTH_SHORT).show();


                }
                if((++startActivity)==4)
                {//akshay
                    //startActivity(new Intent(MainActivity.this, com.astuetz.viewpager.extensions.caravan.MainActivity.class));
                    //finish();
                }
            }
        }.execute(location);
    }
//End data

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
  /*      if (id == R.id.action_refresh) {
            adapter.clear();
            getData();
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    public void getData()
    {
        TaxiApp.gettaxiPrices().clear();
        getDataFromUBER();
        getDataFromTaxiForSure();
        getDataFromMeru();
        getDataFromOla();

        //


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Toast.makeText(getApplicationContext(),"back",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, DetailActivity.class));
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        startActivity(new Intent(this, DetailActivity.class));
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startActivity(new Intent(this, DetailActivity.class));
        finish();
    }

}