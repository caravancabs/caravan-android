package taxidemo.mainview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.caravan.app.R;

import taxidemo.TaxiApp;
import taxidemo.db.model.TaxiPrice;
import taxidemo.db.model.TaxiTime;

/*
    Array Adapter : Rendering single taxi item in list
 */
public class TaxiAdapter extends ArrayAdapter<TaxiTime> {

    String taxicompany;
    boolean flag=true;
    private static final String KM = " Km";
    private Context context;

    public TaxiAdapter(Context context,String taxiCompany) {
        super(context, R.layout.item_taxi, TaxiApp.gettaxiTimes(taxiCompany));
      this.taxicompany=taxiCompany;
        this.context = context;

    }

    @Override
    public int getCount() {
        return TaxiApp.gettaxiTimes(taxicompany).size();
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {


        final TaxiTime taxiTime = getItem(position);

        View row;

        LayoutInflater inflator = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            row = inflator.inflate(R.layout.item_taxi, parent, false);
        else
            row = convertView;

        final TextView companyName = (TextView) row.findViewById(R.id.taxi_company_name);
        final TextView modelName = (TextView) row.findViewById(R.id.taxi_model_name);
        modelName.setText(taxiTime.getModelName());
        final TextView time = (TextView) row.findViewById(R.id.taxi_eta);
        Integer timeInsec = taxiTime.getTimeInSec();

        final TextView price= (TextView) row.findViewById(R.id.taxi_price);
        String companyName1 = taxiTime.getCompanyName();
        String estimateAvg = "";

        if (companyName1 == "Uber Cabs") {
            String productId = taxiTime.getProductId();
            TaxiPrice taxiPrice = TaxiApp.getTaxiPriceByProductId(productId);
            try {
                if(taxiPrice.getLowEstimate()!=0.0) {
                    estimateAvg = "₹ " + Double.toString(taxiPrice.getLowEstimate());
                    taxiTime.setPrice(""+estimateAvg);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        } else if (companyName1 == "TaxiForSure") {
      /*      Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
            String strDate = sdf.format(c.getTime());
        Log.d("time",strDate);
*/
            double duration = TaxiApp.getDistance();
            companyName1="TFS";

            duration *= 1.60934;

            String modelName1 = taxiTime.getModelName();
            double estimate = 0;
            if (modelName1.toLowerCase().contains("hatchback")) {
                estimate = duration * 14;
            } else if (modelName1.toLowerCase().contains("sedan")) {
                estimate = duration * 16;
            } else {
                estimate = duration * 13;
            }
            if (estimate <= 56) {
                estimate = 49;
            }
            estimateAvg = "₹ " + Double.toString(Math.round(estimate));
            taxiTime.setPrice(""+estimateAvg);
        }

        else if (companyName1 == "Ola") {

            double duration = TaxiApp.getDistance();

            duration *= 1.60934;

            String modelName1 = taxiTime.getModelName();
            Log.d("Model", modelName1);
            double estimate = 0;
            if (modelName1.toLowerCase().contains("sedan")) {

                estimate = 150;

                if (duration > 6) {
                    duration -= 6;
                    estimate += duration * 16;
                }
            } else if (modelName1.toLowerCase().contains("mini")) {
                estimate = 100;

                if (duration > 4) {
                    duration -= 4;
                    estimate += duration * 10;
                }
            } else if (modelName1.toLowerCase().contains("prime")) {
                estimate = 200;

                if (duration > 5) {
                    duration -= 5;
                    estimate += duration * 20;
                }
            }

            else if (modelName1.toLowerCase().contains("auto")) {

                estimate=0;
            }

if(estimate!=0)
            estimateAvg = "₹ " + Double.toString(Math.round(estimate));
            else
    estimateAvg="Metered";
            taxiTime.setPrice(""+estimateAvg);
        }


        else if(companyName1.contains("Meru"))
        {
            Log.d("Price","Meru");

            double duration = TaxiApp.getDistance();

            duration *= 1.60934;

            double estimate = 0;
            String modelName1=taxiTime.getModelName();
        if(modelName1.contains("ru"))
        {
            estimate=duration*15;
            if(estimate<100)
                estimate=99;
            else estimate+=39;
        }
            else
        {
            estimate=duration*14;
            if(estimate<100)
                estimate=99;
            else estimate+=29;
        }




            if(estimate!=0)
                estimateAvg = "₹ " + Double.toString(Math.round(estimate));
            else
                estimateAvg="";
            taxiTime.setPrice(""+estimateAvg);
        }
//  end pricing

        int min = timeInsec / 60;
        int sec = timeInsec % 60;

        String formattedTime =  Integer.toString(min)  ;// + sec + " sec";




        time.setText(formattedTime+" min");
        if(estimateAvg!="")
            estimateAvg=estimateAvg.substring(0,estimateAvg.length()-2);
        price.setText( estimateAvg);

        ImageView icon = (ImageView) row.findViewById(R.id.taxi_company_icon);
        icon.setImageResource(taxiTime.getDrawable());
      //  companyName.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
      //  companyName.setText(companyName1);


        final String companyName2=taxiTime.getCompanyName();
       // Button b = (Button) row.findViewById(R.id.taxi_button);
       /* b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }



        });
*/
/*
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //FragmentManager fragmentManager=
                String packageName="";
                if(companyName2== "Uber Cabs")
                {
                    packageName="com.ubercab";
                }
                else if (companyName2=="TaxiForSure")
                {
                    packageName="com.tfs.consumer";
                }
                else if (companyName2 == "Ola")
                {
                    packageName="com.olacabs.customer";
                }
                else if(companyName2=="Meru Cabs")
                {
                    packageName="com.winit.merucab";
                }

                //  REDIRECT TO APP
                boolean installed  =   appInstalledOrNot(packageName);
                if(installed) {
                    //This intent will help you to launch if the package is already installed
                    Intent LaunchIntent = context.getPackageManager()
                            .getLaunchIntentForPackage(packageName);
                    context.startActivity(LaunchIntent);

                    System.out.println("App already installed on your phone");
                }
                else {
                    System.out.println("App is not installed on your phone");
                    try {
                        //context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(")));
                        Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=" + packageName));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    } catch (android.content.ActivityNotFoundException anfe) {
                        Log.d("Not","Accessible");
                    }}

            }
            private boolean appInstalledOrNot(String uri) {
                PackageManager pm = context.getPackageManager();
                boolean app_installed = false;
                try {
                    pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                    app_installed = true;
                }
                catch (PackageManager.NameNotFoundException e) {
                    app_installed = false;
                }
                return app_installed ;
            }
        });
*/
        return row;
    }




}