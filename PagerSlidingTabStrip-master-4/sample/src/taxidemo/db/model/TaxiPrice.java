package taxidemo.db.model;


public class TaxiPrice {

    String productId;
    Double distance;
    Integer duration;
    String displayName;
    int surgeMulti;
    Double highEstimate;
    Double lowEstimate=0.0;
    String currencyCode;
    String estimateAvg;

    public TaxiPrice(String productId, Double distance,
                     Integer duration, String displayName,
                     int surgeMulti, double highEstimate,
                     double lowEstimate, String currencyCode,
                     String estimateAvg) {
        this.productId = productId;
        this.distance = distance;
        this.duration = duration;


        this.displayName = displayName;
        this.surgeMulti = surgeMulti;
        this.highEstimate = highEstimate;
        this.lowEstimate = lowEstimate;
        this.currencyCode = currencyCode;
        this.estimateAvg = estimateAvg;
    }

    @Override
    public String toString() {
        return "ProductId : " + productId + " Dist : " + distance +" Duration : "+ duration + " EstimateAvg : "+estimateAvg;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setHighEstimate(Double highEstimate) {
        this.highEstimate = highEstimate;
    }

    public void setLowEstimate(Double lowEstimate) {
        this.lowEstimate = lowEstimate;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getSurgeMulti() {
        return surgeMulti;
    }

    public void setSurgeMulti(int surgeMulti) {
        this.surgeMulti = surgeMulti;
    }

    public double getHighEstimate() {
        return highEstimate;
    }

    public void setHighEstimate(double highEstimate) {
        this.highEstimate = highEstimate;
    }

    public double getLowEstimate() {
        return lowEstimate;
    }

    public void setLowEstimate(double lowEstimate) {
        this.lowEstimate = lowEstimate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getEstimateAvg() {
        return estimateAvg;
    }

    public void setEstimateAvg(String estimateAvg) {
        this.estimateAvg = estimateAvg;
    }
}
