package taxidemo.db.model;

/*
    Container class for single taxi
 */
public class TaxiTime {

    String productId;
	String companyName;
    String modelName;
	Integer timeInSec;
    String price;
	Integer drawable;

    public TaxiTime(String productId, String companyName, String modelName, Integer timeInSec) {
        this.productId = productId;
        this.companyName = companyName;
        this.modelName = modelName;
        this.timeInSec = timeInSec;
        this.price = "0";
    }

    public TaxiTime(String productId, String companyName, String modelName, Integer timeInSec, String price) {
        this.productId = productId;
        this.companyName = companyName;
        this.modelName = modelName;
        this.timeInSec = timeInSec;
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getTimeInSec() {
        return timeInSec;
    }

    public void setTimeInSec(Integer timeInSec) {
        this.timeInSec = timeInSec;
    }

    public Integer getDrawable() {
        return drawable;
    }

    public void setDrawable(Integer drawable) {
        this.drawable = drawable;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String drawable) {
        this.price = drawable;
    }
}
