package taxidemo.DetailView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.caravan.app.MainActivity;
import com.caravan.app.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import taxidemo.TaxiApp;
import taxidemo.cabs.Uber;
import taxidemo.utils.AndroidLocationManager;
import taxidemo.utils.GeoCoder;


public class DetailActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    TextView resDuration, resDistance, resPrice;
    AutoCompleteTextView srcAddr, destAddr;
    Button buttonGo;
    ProgressBar progressBar;
    String productId;
    String srcText, destText;
    AndroidLocationManager manager;
    String srcAddress, destAddress;
    LinearLayout resultLayout,ll_gobt;
    boolean sourceAddressChanged = false;
    Location srcLoc,src ,srcLocation,destLocation;

    ////Akshay Edit
    //TextView tv_mainTitle ;
    ///


    double Mylat, Mylong;

    private static final String LOG_TAG = "Google Places";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyAwuG3oBOLsP5rbBc6azk9ZmJrNd5McXrs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Link UI
        srcAddr = (AutoCompleteTextView) findViewById(R.id.et_src_addr);
        destAddr = (AutoCompleteTextView) findViewById(R.id.et_dest_addr);

        ////Akshay Edit
        //tv_mainTitle = (TextView) findViewById(R.id.textView_maintitle);
        //add font
        //Typeface myTypeFace = Typeface.createFromAsset(getAssets(),"/fonts/robotolight.ttf");
        //tv_mainTitle.setTypeface(myTypeFace);

        ll_gobt = (LinearLayout)findViewById(R.id.ll_bt_go);
        ll_gobt.setClickable(true);

        srcAddr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                srcAddr.setText("");
            }
        });      //  buttonGo = (TextView) findViewById(R.id.button_go);
       // buttonGo.setOnClickListener(this);

        ll_gobt.setOnClickListener(this);

        srcAddr.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_items));
        srcAddr.setOnItemClickListener(this);

        destAddr.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_items));
        destAddr.setOnItemClickListener(this);


        manager = new AndroidLocationManager(this);

        TaxiApp taxiapp = (TaxiApp) getApplicationContext();

        srcLoc = taxiapp.getUserLocation();
        // Set source location
        setSourceAddress();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
/*        try {
            this.finalize();
            System.exit(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }*/
        this.finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        destAddr = (AutoCompleteTextView) findViewById(R.id.et_dest_addr);
        destAddr.setText("");

        destAddr.requestFocus();
        InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm1.showSoftInput(destAddr, InputMethodManager.SHOW_IMPLICIT);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.ll_bt_go) {

            Vibrator vi = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vi.vibrate(100);


            //ll_gobt.setBackgroundColor(Color.parseColor("#FFAE3BE9"));

            srcAddress = srcAddr.getText().toString();
            destAddress = destAddr.getText().toString();

            TaxiApp.destAddress=destAddress;
            Log.d("destAddress", destAddress);
            putTaxiPriceInList(sourceAddressChanged);

        }
    }
    private void setSourceAddress() {

        new AsyncTask<Location, Void, String>() {

            @Override
            protected String doInBackground(Location... params) {

                try {

                    LatLng loc = new LatLng(params[0].getLatitude(), params[0].getLongitude());
                    String addr = GeoCoder.getAddress(DetailActivity.this, loc);

                    return addr;
                } catch (IOException e) {
                    Log.d("Failed at geocoder", e.toString());
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String aVoid) {
                srcAddr.setText(aVoid);
            }
        }.execute(srcLoc);
    }
    private void putTaxiPriceInList(final boolean isNewLocation) {         // get taxi list from Uber

        new AsyncTask<Location, Void, Boolean>() {


            @Override
            protected Boolean doInBackground(Location... params) {

                //    UNDER CONSTRUCTION

                Double srcLat = null,srcLong=null,destLong=null,destLat=null;
                try {
                    Uber uber = null;
                    if (isNewLocation || true) {
                        //   src = GeoCoder.getLocation(DetailActivity.this, srcAddress);

                        Geocoder coder = new Geocoder(DetailActivity.this);
                        List<Address> address = null;

                        try {
                            address = coder.getFromLocationName(srcAddress, 1);
                            if (address == null) {
                                Log.d("src location ","NULL");
                                return null;
                            }
                            Address location = address.get(0);
                            srcLat=(location.getLatitude());
                            srcLong=(location.getLongitude());
                            //  src=GeoCoder.get
                            uber = new Uber(src);

                        } catch (IOException e) {
                            Log.d("fail", " price src location");


                        } finally {
                            //   DESTINATION WORK

                            Log.e("start ","Destination work");
                            address.clear();


                            try {
                                address = coder.getFromLocationName(destAddress, 1);
                                if (address == null) {

                                    return null;
                                }

                                Address location2 = address.get(0);


                                destLat = location2.getLatitude();
                                destLong = location2.getLongitude();

                                srcLocation=new Location("source");
                                srcLocation.setLatitude(srcLat);
                                srcLocation.setLongitude(srcLong);
                                destLocation=new Location("Dest");
                                destLocation.setLatitude(destLat);
                                destLocation.setLongitude(destLong);

                                TaxiApp taxiapp = (TaxiApp) getApplicationContext();
                                taxiapp.setSrcLocation(srcLocation);
                                taxiapp.setDestLocation(destLocation);
                                TaxiApp.gettaxiPrices().clear();
                                TaxiApp.refresh=true;
                                Log.d("very", "close");
                                //  uber.getPrice(srcLat,srcLong,destLat,destLong);
                                //   uber.putTaxiPriceInList(TaxiApp.gettaxiPrices(),srcLat,srcLong,destLat,destLong);

                                return true;
                            } catch (Exception e) {
                                Log.d("Problem", " yes");
                            }
                            return false;
                        }
                    }


                } catch (Exception e) {
                    Log.d("failed in: ", " putTaxi");
                }

                return true;
                //  under construction
            }

            @Override
            protected void onPostExecute(Boolean res) {


                startActivity(new Intent(DetailActivity.this, MainActivity.class));
                finish();
//                progressBar.setVisibility(View.GONE);

                if (res == null || res == false) {
                    Toast.makeText(DetailActivity.this, "Failed to Load data for Uber", Toast.LENGTH_SHORT).show();
                    return;
                }


            }


        }.execute(srcLoc);
    }




    @Override
    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
     /*   if (view.getId() == R.id.et_dest_addr) {
            srcText = (String) adapterView.getItemAtPosition(position);

        } else
        {
                destText=(String) adapterView.getItemAtPosition(position);
           Toast.makeText(this, destText, Toast.LENGTH_SHORT).show();

        }*/

    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:IN");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index).toString();
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

}